//Last Edit 2-16-14
//This function checks to see if collision queue exists already, if 
//it doesn't, it make one and checks the area of circle defined by the arguments
//passed into for collisions with all the objects within it and stores it into
//a global variable.  If a queue already exists, then it pops the first id
//off the list and saves it into a global variable.  Once all the id from the 
//queue are popped off, it deletes it to clear up memory

var iid, temp;

if (!global.using_bubble_collisions){ //new list
    global.__list = ds_queue_create();
    temp = ds_queue_create();
    do{
        iid = collision_circle(argument0,argument1,argument2,argument3,argument4,argument5);
        instance_deactivate_object(iid);
        if iid > -1{
            ds_queue_enqueue(global.__list,iid)
            instance_deactivate_object(iid);
        }
    }
    until (iid<0)
    ds_queue_copy(temp,global.__list);
    while ds_queue_size(global.__list)>0{
        instance_activate_object(ds_queue_dequeue(global.__list));
    }
    ds_queue_copy(global.__list,temp);
    global.using_bubble_collisions = true;
}
{ //otherwise there is an active list
    if ds_queue_size(global.__list)>0{
        global._id = ds_queue_dequeue(global.__list);
        return 1;
    }
    else{ //end of list
        ds_queue_destroy(global.__list);
        global.using_bubble_collisions = false;
        global.__list = -1;
        return 0;
    }
}
