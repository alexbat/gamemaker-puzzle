
//Last Edit 3-16-14
//Destroys the surface and the object tied to the surface area
//then it remakes the surface for future usage 

global.active_surface = false;
with (obj_time_bubble)
  {
    instance_destroy();
  }
surface_free(obj_character.time_bubble);



//obj_character.time_bubble = 0;

//Remakes the surface
obj_character.time_bubble = surface_create(300,300);

obj_character.new_surf = true;
