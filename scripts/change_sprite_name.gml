//Last Edit 2-16-14
//This function checks to see if the name passed into it 
//has the specified time region in its sprite name
//if it does, then it changes it to the time zone passed in, otherwise
//it returns false

var new_name, position, substr;

//Make substr match the current time zone
switch(global.time_zone){
  case -1: substr = "Past"; break;
  case 0: substr = "Present"; break;
  case 1: substr = "Future"; break;
}

//check to see if substr is in the string passed in
position = string_pos(substr, argument1)
if ( position != 0){

  //Delete the found substr and replace it with the string passed in
  new_name = string_delete(argument1, position, string_length(substr));
  return (string_insert(argument0, new_name, position));
  
}
return ("false");
