//Turn on Time Bubble Surface and save the characters coordinates at this instance to 
//create the bubble at those points

if (global.time_zone != argument1 && argument2){
  global.active_surface = true;
  global.time_button_pressed = true;
  global.time_zone_pressed = argument0;
  obj_character.bubble_x = obj_character.x;
  obj_character.bubble_y = obj_character.y;
  
  //Turn off the time buttons when bubble active
  with (obj_pastButton){
    image_alpha = 0;
    touchable = false;
  }
  with (obj_presentButton){
    image_alpha = 0;
    touchable = false;
  }
  with (obj_futureButton){
    image_alpha = 0;
    touchable = false;
  }
  
  //Turn on the Bubble buttons since bubble active now
  with (obj_enter){
    image_alpha = 1;
    touchable = true;
  }
  with (obj_cancel){
    image_alpha = 1;
    touchable = true;
  }
}

