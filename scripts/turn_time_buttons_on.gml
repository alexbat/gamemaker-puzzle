//Last Edit 3-24-14
//Turns the time buttons back on and turn the bubble buttons off now 

with (obj_pastButton){
  image_alpha = 1;
  touchable = true;
}
with (obj_presentButton){
  image_alpha = 1;
  touchable = true;
}
with (obj_futureButton){
  image_alpha = 1;
  touchable = true;
}

//Turn off the Bubble buttons since bubble destroyed 
  with (obj_enter){
    image_alpha = 0;
    touchable = false;
  }
  with (obj_cancel){
    image_alpha = 0;
    touchable = false;
  }


