//Last edit 1-21-14
//When a button to change time zones is pressed, the global time zone variable is called
//to change to the new time zone

//First Checks to make sure the character isn't in a state where the user needs
//to wait for an event to end before he can move the character again and thus
//also change time zones

//CHANGES 3-15-14
//Removed the check to see if in the same time zone since that check is
//already done when creating the time bubble, therefore this check would 
//be redundant, can bring back if decide to go about it differently

if (global.lock_character == false){

  //Change the time zone and increment number of times changed
  global.time_zone = argument0;
  global.timeChangeCount++;
  
};
