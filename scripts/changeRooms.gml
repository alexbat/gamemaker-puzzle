//Last edit 1-13-14
//(Old Code/Not being used) 
//When button are pressed, checks the current time zone with the designated one to go to
//and changes rooms accordingly else if already in the same room, do nothing.

var change_room;

if (button_id != global.time_zone) {
  if (button_id == PAST) {
    //change to past room
    change_room = PAST;
  }
  else if (button_id == PRESENT){
    //change to present room
    change_room = PRESENT;
  }
  else {
    //change to future room
    change_room = FUTURE;
  };
  
  //Now jump into the room according to the button id
  switch (change_room) {
    case -1: room_goto(rm_pastProto); break;
    case 0: room_goto(rm_presentProto); break;
    case 1: room_goto(rm_futureProto); break;
    default: break;
  };
}

