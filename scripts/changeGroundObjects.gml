//Last edit 1-13-14
//(Old code/Not being used)
//Change ground object to match time zone unless already in the time zone user wants to
//travel to, then ignore function call

if (global.time_zone != ground_time_zone){
  if (global.time_zone == PAST){
    //Change the ground to a past ground object
    instance_change(obj_pastGround, 1);
  }
  else if (global.time_zone == PRESENT){
    //Change the ground to a present ground object
    instance_change(obj_presentGround, 1);
  }
  else{
    //Change the ground to a future ground object
    instance_change(obj_futureGround, 1);
  }
}
