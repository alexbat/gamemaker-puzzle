//Last Edit 3-20-14
//cover_outside_time_bubble(void);

//Outlines the time zone with a circle and fills the outside of the
//bubble with triangles to get rid of any sprites sticking out 



draw_set_blend_mode_ext(bm_zero, bm_zero);

//REMOVE THIS WHEN DONE, USING THIS TO TEST THINGS OUT
//draw_set_color(c_aqua);

//Top Left
draw_triangle(0, 0, 89, 0, 0, 89, false);

draw_triangle(0, 89, 0, 134, 20, 69, false);
draw_triangle(19, 69, 17, 78, 29, 60, false);

draw_triangle(89, 0, 134, 0, 69, 20, false);
draw_triangle(68, 20, 58, 31, 77, 20, false);

//Top Right
draw_triangle(300, 0, 211, 0, 300, 89, false);

draw_triangle(211, 0, 166, 0, 231, 20 ,false);
draw_triangle(230, 19, 226, 20, 240, 33, false);

draw_triangle(300, 89, 300, 134, 280, 69, false);
draw_triangle(280, 69, 282, 77, 271, 60, false);

//Bottom Left
draw_triangle(0, 300, 0, 211, 89, 300, false);

draw_triangle(0, 211, 0, 166, 20, 231, false);
draw_triangle(19, 231, 17, 220, 29, 239, false);

draw_triangle(89, 300, 134, 300, 69, 280, false);
draw_triangle(68, 280, 60, 272, 73, 279, false);

//Bottom Right
draw_triangle(300, 300, 211, 300, 300, 211, false);

draw_triangle(211, 300, 166, 300, 231, 280, false);
draw_triangle(231, 281, 226, 279, 238, 272, false);

draw_triangle(300, 211, 300, 166, 280, 231, false);
draw_triangle(278, 226, 281, 232, 271, 237, false);

//draw_set_color(c_fuchsia);
draw_triangle(282, 222, 281, 232, 269, 239, false);


draw_set_blend_mode(bm_normal);

draw_set_colour(c_black);



draw_circle(150, 150, 150, true);
draw_circle(150, 150, 149, true);
draw_circle(150, 150, 151, true);


